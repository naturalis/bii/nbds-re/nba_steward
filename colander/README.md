## Steward: Colander

This subset of scripts takes care of moving jobs and data files from a AWS S3 bucket, where they are waiting to be processed, to the Validator and next to the Infuser. When a job has finished, reports of the job will be moved back to the AWS S3 bucket.

`deploy_dirs.sh` : is a providing script that will create the necessary directories, based on a config file (deploy_dirs.cfg).

`pull_new_jobs.sh` : downloads new jobs (job files) from the AWS S3 bucket. When a job file has been downloaded, the file in the AWS S3 bucket will be moved from the `new` to the `in_progress` folder.

`move_job_to_validator.sh` : before moving the job file to the validator, this script will download the data files needed for this job. When the download has completed, job file and data files will be moved to the "new jobs folder" of the Validator.
