#!/bin/bash
#
# Usage: ./deploy_dirs [param]
# 
# Optional parameters:
# --debug : show all messages
# --warn  : show info and warning messages
#
# E.g.: ./deploy_dirs --warn
#
# 
# Mandartory environment variables:
#
# CONFIG_REPO           : HTTPS clone URL of the gitlab repo
# CONFIG_REPO_FILE      : name of configuration file, INCLUDING the relative path
# VALIDATOR_DATA        : the data directory for the validator, e.g. /data/validator
#
# Tom Gilissen
# June 2020



source functions.sh

exit 0





### Variables and Constants

config_repo_dir=/etc/steward/config

debug=false
warn=false
errors=0
warnings=0
fail_on_warning=false

declare -a valid_doc_types=( multimedia specimen taxon )


### Functions

is_valid_doc_type() {
	valid=false
	for valid_doc_type in "${valid_doc_types[@]}"; do
		if [ $1 = $valid_doc_type ]; then
			valid=true
		fi
	done
	if [ $valid = false ]; then
		exit_on_error "invalid doc_type: $1. Config file is invalid!"
	fi
}

load_script_props() {
	if [ -e deploy_dirs.cfg ]; then
		source deploy_dirs.cfg
	fi
}

is_config_available() {
	CONFIG_REPO_FILE=$config_repo_dir/$STEWARD_CONFIG_REPO_FILE
	if [ ! -e $CONFIG_REPO_FILE ]; then
		exit_on_error "missing config file: $STEWARD_CONFIG_REPO_FILE"
	fi
	log_debug "config file used: $STEWARD_CONFIG_REPO_FILE"
}


### Main 

begin=`date +"%Y-%m-%d %T"`
log_info "started at $begin"

# Extra logging
if [ ! -z $1 ] && [ $1 = "--debug" ]; then
	debug=true
	warn=true
fi
if [ ! -z $1 ] && [ $1 = "--warn" ]; then
	warn=true
fi

load_script_props
is_config_available


# Check configuration

log_debug "checking the configuration file: $STEWARD_CONFIG_REPO_FILE"

# Verify if the config file is valid YAML
test=$( yq r -j $CONFIG_REPO_FILE ) # Workaround for 'yq v config.yml'
if [ $? -gt 0 ]; then
	exit_on_error "invalid yaml! Execution ended"
fi

# Check if data suppliers have been defined
data_suppliers=$( yq r $CONFIG_REPO_FILE 'data_suppliers.*.name' )
if [ ${#data_suppliers} -eq 0 ]; then
	exit_on_error "no data suppliers defined. Check the config file!"
fi

# Check doc types and paths
for supplier in $data_suppliers;
do
	log_debug "data supplier: $supplier"

	# doc types
	doc_types=$( yq r $CONFIG_REPO_FILE 'data_suppliers.(name=='$supplier').doc_type.*' )
	if [ ${#doc_types} -eq 0 ]; then
		exit_on_error "no doc_types defined for data supplier: $supplier. Check the config file!"
		continue
	fi
	for type in $doc_types;
	do
		is_valid_doc_type $type
	done

	# path
	path=$( yq r $CONFIG_REPO_FILE 'data_suppliers.(name=='$supplier').path' )
	log_debug "- path: $path"
	if [ -z $path ]; then
		exit_on_error "missing path for data supplier: $supplier. Check the config file!"
	fi

done
log_debug "configuration is OK"


# Everything seems to be fine so we can start to (re)create directories

log_debug "(re)creating directories"
for supplier in $data_suppliers;
do
	## re-create base directory
	path=$( yq r $CONFIG_REPO_FILE 'data_suppliers.(name=='$supplier').path' )
	path=${path%/}; # remove trailing forward slash
	log_debug "mkdir -p $path"
	error=$( mkdir -p $path 2>&1)
	if [ $? -gt 0 ]; then
		log_error "failed to create \"$path\" for data supplier $supplier (\"$error\")"
	fi

	# Create default directory
	log_debug "mkdir -p $path/{archive,reports}"
	error=$( mkdir -p $path/{archive,reports}  2>&1)
	if [ $? -gt 0 ]; then
		log_error "failed to create archive/reports directories for data supplier $supplier (\"$error\")"
	fi

	# Create directory for each doc_type
	doc_types=$( yq r $CONFIG_REPO_FILE 'data_suppliers.(name=='$supplier').doc_type.*' )
	for type in $doc_types;
	do
		log_debug "mkdir -p $path/$type"
		error=$( mkdir -p $path/$type 2>&1)
		if [ $? -gt 0 ]; then
			log_error "failed to create \"$path\" for data supplier $supplier (\"$error\")"
		fi
	done
done

end=`date +"%Y-%m-%d %T"`
log_info "finished at $end with $errors error(s) and $warnings warning(s)"
exit $errors
