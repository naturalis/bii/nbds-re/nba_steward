#!/bin/bash
#
# entrypoint.sh : the docker image main command
#
# Tom Gilissen
# Februari 2021

source functions.sh

# Verify if the mandatory environment variables have been provided

if [[ -z $AWS_S3_ACCESS_KEY_ID ]]; then
		exit_on_error "Missing env variable: AWS_S3_ACCESS_KEY_ID"
fi
if [[ -z $AWS_S3_SECRET_ACCESS_KEY ]]; then
		exit_on_error "Missing env variable: AWS_S3_SECRET_ACCESS_KEY"
fi
if [[ -z $AWS_S3_DEFAULT_REGION ]]; then
		exit_on_error "Missing env variable: AWS_S3_DEFAULT_REGION"
fi
if [[ -z $AWS_S3_BUCKET ]]; then
		exit_on_error "Missing env variable: AWS_S3_BUCKET"
fi
if [[ -z $GITLAB_CONFIG_PRIVATE_KEY_BASE64 ]]; then
		exit_on_error "Missing env variable: GITLAB_CONFIG_PRIVATE_KEY_BASE64"
fi
if [[ -z $GITLAB_CONFIG_REPO ]]; then
		exit_on_error "Missing env variable: GITLAB_CONFIG_REPO"
fi
if [[ -z $STEWARD_CONFIG_REPO_FILE ]]; then
		exit_on_error "Missing env variable: STEWARD_CONFIG_REPO_FILE"
fi
if [[ -z $STEWARD_DATA ]]; then
		exit_on_error "Missing env variable: STEWARD_DATA"
fi
if [[ -z $VALIDATOR_INCOMING_JOB_FOLDER ]]; then
		exit_on_error "Missing env variable: VALIDATOR_INCOMING_JOB_FOLDER"
fi
if [[ -z $VALIDATOR_OUTGOING_JOB_FOLDER ]]; then
		exit_on_error "Missing env variable: VALIDATOR_OUTGOING_JOB_FOLDER"
fi
if [[ -z $VALIDATOR_VALIDATED_OUTPUT_FOLDER ]]; then
		exit_on_error "Missing env variable: VALIDATOR_VALIDATED_OUTPUT_FOLDER"
fi
if [[ -z $INFUSER_DATA ]]; then
		exit_on_error "Missing env variable: INFUSER_DATA"
fi


# Set the environment variables for all users

echo "AWS_S3_ACCESS_KEY_ID=$AWS_S3_ACCESS_KEY_ID" >> /etc/environment
echo "AWS_S3_SECRET_ACCESS_KEY=$AWS_S3_SECRET_ACCESS_KEY" >> /etc/environment
echo "AWS_S3_DEFAULT_REGION=$AWS_S3_DEFAULT_REGION" >> /etc/environment
echo "AWS_S3_BUCKET=$AWS_S3_BUCKET" >> /etc/environment
echo "GITLAB_CONFIG_PRIVATE_KEY_BASE64=$GITLAB_CONFIG_PRIVATE_KEY_BASE64" >> /etc/environment
echo "GITLAB_CONFIG_REPO=$GITLAB_CONFIG_REPO" >> /etc/environment
echo "STEWARD_CONFIG_REPO_FILE=$STEWARD_CONFIG_REPO_FILE" >> /etc/environment
echo "STEWARD_DATA=$STEWARD_DATA" >> /etc/environment
echo "VALIDATOR_INCOMING_JOB_FOLDER=$VALIDATOR_INCOMING_JOB_FOLDER" >> /etc/environment
echo "VALIDATOR_OUTGOING_JOB_FOLDER=$VALIDATOR_OUTGOING_JOB_FOLDER" >> /etc/environment
echo "VALIDATOR_VALIDATED_OUTPUT_FOLDER=$VALIDATOR_VALIDATED_OUTPUT_FOLDER" >> /etc/environment
echo "INFUSER_DATA=$INFUSER_DATA" >> /etc/environment

# Configure git

# 1. add credentials on build, Certificate environment variable SSH_PRIVATE_KEY is Base64 encoded!
mkdir -p /root/.ssh/
echo $GITLAB_CONFIG_PRIVATE_KEY_BASE64 | base64 --decode > /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa

# 2. make sure your domain is accepted
touch /root/.ssh/known_hosts
ssh-keyscan gitlab.com >> /root/.ssh/known_hosts

# 3. create config dir
mkdir -p /etc/steward
mkdir -p /etc/steward/log

# Initialise the cron log file
touch /etc/steward/log/steward_colander.log

# Start the run once job
log_info "container has started"

# Setup the cron schedule
crontab /home/ubuntu/crontab
# ... and start cron
cron -f &

# Follow the log file so it shows up in the container log
tail --follow /etc/steward/log/steward_colander.log
