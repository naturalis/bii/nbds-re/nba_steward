## Steward - Source

Core scripts under Steward Source:

- deploy_dirs.sh : creates the directory structure needed to be fully functional. The structure can be configured with the file data_suppliers.yml
- prepare_jobs.sh : check if new data files have been added. If so, it will create a job file that will accompany the data files through the data flow, and push the data files to an AWS S3 bucket where it can be retrieved by other services.

Supporting script:
- upload_job.sh : uploads a job file including it's data directory to an aws bucket


