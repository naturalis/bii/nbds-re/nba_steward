#!/bin/bash
#
# Usage: ./deploy_dirs [parameter]
# 
# Optional parameters:
# --debug : show all messages
# --warn  : show info and warning messages
#
# E.g.: ./deploy_dirs --warn
#
# 
# Mandartory environment variables:
#
# CONFIG_REPO                : HTTPS clone URL of the gitlab repo
# DATA_SUPPLIERS_CONFIG_FILE : configuration file containing metadata about the data suppliers (YAML)
# 
# Tom Gilissen
# October 2021

source functions.sh

### Variables and Constants

steward_config_dir=/etc/steward/config
DATA_SUPPLIERS_CONFIG_FILE=$STEWARD_SOURCE_DATA_SUPPLIERS
tmp_dir=/data/tmp

debug=false
warn=false
errors=0
warnings=0
fail_on_warning=false

declare -a valid_doc_types=( multimedia specimen taxon )


### Functions

is_valid_doc_type() {
  valid=false
  for valid_doc_type in "${valid_doc_types[@]}"; do
    if [ $1 = $valid_doc_type ]; then
      valid=true
    fi
  done
  if [ $valid = false ]; then
    exit_on_error "invalid doc_type: $1. Config file is invalid!"
  fi
}

load_script_props() {
  if [ -e deploy_dirs.cfg ]; then
    source deploy_dirs.cfg
  fi
}

is_config_available() {
  DATA_SUPPLIERS_CONFIG_FILE=$steward_config_dir/$DATA_SUPPLIERS_CONFIG_FILE
  if [ ! -e $DATA_SUPPLIERS_CONFIG_FILE ]; then
    exit_on_error "missing config file: $DATA_SUPPLIERS_CONFIG_FILE"
  fi
  log_debug "config file used: $DATA_SUPPLIERS_CONFIG_FILE"
}


### Main 

# Extra logging
if [ ! -z $1 ] && [ $1 = "--debug" ]; then
  debug=true
  warn=true
fi
if [ ! -z $1 ] && [ $1 = "--warn" ]; then
  warn=true
fi

load_script_props
is_config_available

# Check configuration

log_debug "checking the configuration file: $DATA_SUPPLIERS_CONFIG_FILE"

# Verify if the config file is valid YAML
test=$( yq eval 'true' $DATA_SUPPLIERS_CONFIG_FILE )
if [ $? -gt 0 ]; then
  exit_on_error "invalid yaml! Execution ended"
fi

# Check if data suppliers have been defined
data_suppliers=$( yq eval '.data_suppliers[].name' $DATA_SUPPLIERS_CONFIG_FILE )
if [ ${#data_suppliers} -eq 0 ]; then
  exit_on_error "no data suppliers defined. Check the config file!"
fi

# Check doc types and paths
for supplier in $data_suppliers;
do
  log_debug "data supplier: $supplier"

  # doc types
  doc_types=$( yq eval '.data_suppliers[] | select(.name == "'$supplier'") | .doc_type[]' $DATA_SUPPLIERS_CONFIG_FILE )
  if [ ${#doc_types} -eq 0 ]; then
    exit_on_error "no doc_types defined for data supplier: $supplier. Check the config file!"
    continue
  fi
  for type in $doc_types;
  do
    is_valid_doc_type $type
  done

  # path
  path=$( yq eval '.data_suppliers[] | select(.name == "'$supplier'") | .path' $DATA_SUPPLIERS_CONFIG_FILE )
  log_debug "- path: $path"
  if [ -z $path ]; then
    exit_on_error "missing path for data supplier: $supplier. Check the config file!"
  fi

done
log_debug "configuration is OK"


# Everything seems to be fine so we can start to (re)create directories

log_debug "(re)creating directories"
for supplier in $data_suppliers;
do
  # re-create base directory
  path=$( yq eval '.data_suppliers[] | select(.name == "'$supplier'") | .path' $DATA_SUPPLIERS_CONFIG_FILE )
  path=${path%/};
  log_debug "mkdir -p $path"
  error=$( mkdir -p $path 2>&1)
  if [ $? -gt 0 ]; then
    log_error "failed to create \"$path\" for data supplier $supplier (\"$error\")"
  fi

  # Create default directory
  log_debug "mkdir -p $path/{archive,reports}"
  error=$( mkdir -p $path/{archive,reports}  2>&1)
  if [ $? -gt 0 ]; then
    log_error "failed to create archive/reports directories for data supplier $supplier (\"$error\")"
  fi

  # Create directory for each doc_type
  doc_types=$( yq eval '.data_suppliers[] | select(.name == "'$supplier'") | .doc_type[]' $DATA_SUPPLIERS_CONFIG_FILE )
  for type in $doc_types;
  do
    log_debug "mkdir -p $path/$type"
    error=$( mkdir -p $path/$type 2>&1)
    if [ $? -gt 0 ]; then
      log_error "failed to create \"$path\" for data supplier $supplier (\"$error\")"
    fi
  done
done

# (re)create tmp dir
log_debug "mkdir -p $tmp_dir"
error=$( mkdir -p $tmp_dir 2>&1)
if [ $? -gt 0 ]; then
  log_error "failed to create tmp dir \"$tmp_dir\": $error"
fi

log_info "finished with $errors error(s) and $warnings warning(s)"
exit $errors
