#!/bin/bash
#
# upload_job.sh : utility script for uploading a job file
# including it's data directory to an aws bucket
#
# usage: upload_job.sh job-file.json job-dir-name
#
# Tom Gilissen
# Februari 2021

source functions.sh

alias aws='docker run --rm -i -v $MINIO_SOURCE_DATA_DIR:/aws \
         --env AWS_ACCESS_KEY_ID=$AWS_S3_ACCESS_KEY_ID \
         --env AWS_SECRET_ACCESS_KEY=$AWS_S3_SECRET_ACCESS_KEY \
         --env AWS_DEFAULT_REGION=$AWS_S3_DEFAULT_REGION \
         --env AWS_BUCKET=$AWS_S3_BUCKET \
         amazon/aws-cli'

shopt -s expand_aliases

# Are both job file and source directory available?
if [ ! -s /data/tmp/$1 ]; then
	exit_on_error "job file $1 is missing!"
fi

if [ ! -d /data/tmp/$2 ]; then
	exit_on_error "directory $2 does not exists!"
fi

# Upload to AWS

# 1. sync source files
aws s3 sync tmp/$2 $AWS_S3_BUCKET/$2/
error=$? 
if [ $error -gt 0 ]; then
	echo "ERROR: upload of $2 to AWS failed"
	exit $error
fi

# 2. copy job file
aws s3 cp tmp/$1 $AWS_S3_BUCKET/new/$1
error=$?
if [ $error -gt 0 ]; then
	echo "ERROR: upload of $1 to AWS failed"
	exit $error
fi

log_info "job file uploaded to: $AWS_S3_BUCKET/new/$1"
log_info "source files to bucket: $AWS_S3_BUCKET/$2/"