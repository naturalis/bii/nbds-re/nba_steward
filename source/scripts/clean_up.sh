#!/bin/bash
#
# clean_up.sh : remove all stopped containers
#

source functions.sh

docker container prune -f > /dev/null 2>&1

if [ $? -ne 0 ]
then
	log_error "could not remove all stopped containers."
	exit $?
fi
log_info "ok"