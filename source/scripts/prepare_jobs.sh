#!/bin/bash
#
# prepare_jobs.sh checks if data files have been supplied by
# the data suppliers. When a new set of data has been uploaded
# the script will move this set out of focus for the supplier
# in a new folder with a unique folder name and create a job
# file, that will accompany the data set through the colander
# process.
# 
# Usage: ./prepare_jobs
# 
# Extra parameters:
# --debug : include all possible messages
# --warn  : include warning messages
#
# E.g.: ./prepare_jobs --debug
#
# 
# 
# Tom Gilissen
# June 2020

source functions.sh

### Variables and Constants

config_repo_dir=/etc/steward/config
DATA_SUPPLIERS_CONFIG_FILE=$STEWARD_SOURCE_DATA_SUPPLIERS

debug=false
warn=false
errors=0
warnings=0
fail_on_warning=false

declare -a valid_doc_types=(
  multimedia
  specimen
  taxon
  )


date_iso_8601=
id=
job_file=

multimedia_source=false
specimen_source=false
taxon_source=false

status="IN_PROGRESS"
tabula_rasa=false
is_incremental=false

input_dir=
job_dir=
tmp_dir=/data/tmp
UUID=


# For testing only:
# source /path/to/.env

### Functions

load_script_props() {
  if [ -e prepare_jobs.cfg ]; then
    source prepare_jobs.cfg
  fi
}

is_valid_doc_type() {
  valid=false
  for valid_doc_type in "${valid_doc_types[@]}"; do
    if [ $1 = $valid_doc_type ]; then
      valid=true
    fi
  done
  if [ $valid = false ]; then
    exit_on_error "Invalid doc_type: $1. Config file is invalid!"
  fi
}

is_config_available() {
  DATA_SUPPLIERS_CONFIG_FILE=$config_repo_dir/$DATA_SUPPLIERS_CONFIG_FILE
  if [ ! -e $DATA_SUPPLIERS_CONFIG_FILE ]; then
    exit_on_error "Missing config file: $DATA_SUPPLIERS_CONFIG_FILE"
  fi
  log_debug "Config file: $DATA_SUPPLIERS_CONFIG_FILE"
}

set_date() {
  date_iso_8601=`date --iso-8601=seconds`
  echo "INFO: job date: $date_iso_8601"
}

set_id() {
  data_supplier=$1
  local timestamp=$(date +%s%3N)
  local miliseconds=${timestamp:(-3)}
  local datetime=$(date +%Y-%m-%d-%H%M%S$miliseconds)
  local hash=$(echo "${data_supplier,,}$timestamp" | sha256sum)
  local hash=${hash:0:8} # use only the first 8 characters
  id=${data_supplier,,}-$datetime-$hash
  echo "INFO: job id: $id"
}

set_doc_type_source() {
  for type in "$@";
  do
    if [ $type == "multimedia" ]; then
      multimedia_source=true
    fi
    if [ $type == "specimen" ]; then
      specimen_source=true
    fi
    if [ $type == "taxon" ]; then
      taxon_source=true
    fi
  done
}

reset_doc_type_source() {
  multimedia_source=false
  specimen_source=false
  taxon_source=false
}

create_new_job_dir() {
  UUID=$( uuidgen )
  job_dir=$tmp_dir/$UUID
  mkdir -p $job_dir
  log_debug "created job dir: $job_dir"
}

# Move all source files to a temporary job folder ($job_dir) out of 
# sight of the user so they cannot be changed any longer
move_source_files() {

  log_debug "input_dir: $input_dir"
  log_debug "job_dir: $job_dir"

  if [ $multimedia_source == true ]; then
    log_debug "moving multimedia"
    mkdir $job_dir/multimedia
    ls $input_dir/multimedia/ | grep -v upload_ready | xargs -I {} mv $input_dir/multimedia/{} $job_dir/multimedia/
    rm $input_dir/multimedia/upload_ready
  fi

  if [ $specimen_source == true ]; then
    log_debug "moving specimens"
    mkdir $job_dir/specimen
    ls $input_dir/specimen/ | grep -v upload_ready | xargs -I {} mv $input_dir/specimen/{} $job_dir/specimen/
    rm $input_dir/specimen/upload_ready
  fi

  if [ $taxon_source == true ]; then
    log_debug "moving taxa"
    mkdir $job_dir/taxon
    ls $input_dir/taxon/ | grep -v upload_ready | xargs -I {} mv $input_dir/taxon/{} $job_dir/taxon/
    rm $input_dir/taxon/upload_ready
  fi
  echo "INFO: source files have been moved to: $job_dir"
}

restore_source_files() {

  log_info "restoring source files ..."
  log_debug "input_dir: $input_dir"
  log_debug "job_dir: $job_dir"

  mv $job_dir/* $input_dir/.
  rm -R $job_dir
  rm $tmp_dir/$job_file

  log_info "source files have been restored to: $input_dir"
}

archive_source_files() {
  log_info "archiving source files ..."
  log_debug "input_dir: $input_dir"
  log_debug "job_dir: $job_dir"

  mv $job_dir $input_dir/archive/$UUID
  mv $tmp_dir/$job_file $input_dir/archive/$job_file

  log_info "source files have been archived to: $input_dir/archive"
}

# Create two objects containing information about the source data: 
# 
# source_dir_obj :  the directories containing the source files (per type)
#
# source_files_obj : the source files per type
#
set_source_data() {
  source_dir_obj='{}'
  source_files_obj='{}'

  if [ $multimedia_source == true ]; then
    source_dir_obj=$(echo $source_dir_obj | jq '. += {multimedia: "'$UUID'/multimedia"}')
    files=$( ls $job_dir/multimedia -AH )
    for file in $files
    do
      multimedia_files=( "${multimedia_files[@]}" "$UUID/multimedia/$file" )
      if [ ${#multimedia_files[@]} -gt 9 ]; then
        let "more = $( echo $files | wc -w ) - 10"
        multimedia_files[11]=$more"_more_files"
        break
      fi
    done
    multimedia_files=$( printf "\"%s\"\n" ${multimedia_files[@]} | jq -s '.' )
  else
    multimedia_files=null
  fi

  if [ $specimen_source == true ]; then
    source_dir_obj=$(echo $source_dir_obj | jq '. += {specimen: "'$UUID'/specimen"}')
    files=$( ls $job_dir/specimen -AH )
    for file in $files
    do
      if [ ${#specimen_files[@]} -gt 9 ]; then
        let "more = $( echo $files | wc -w ) - 10"
        specimen_files[11]=$more"_more_files"
        break
      fi
      specimen_files=( "${specimen_files[@]}" "$UUID/specimen/$file" )
    done
    specimen_files=$( printf "\"%s\"\n" ${specimen_files[@]} | jq -s '.' )
  else
    specimen_files=null
  fi

  if [ $taxon_source == true ]; then
    source_dir_obj=$(echo $source_dir_obj | jq '. += {taxon: "'$UUID'/taxon"}')
    files=$( ls $job_dir/taxon -AH )
    for file in $files
    do
      if [ ${#taxon_files[@]} -gt 9 ]; then
        let "more = $( echo $files | wc -w ) - 10"
        taxon_files[11]=$more"_more_files"
        break
      fi
      taxon_files=( "${taxon_files[@]}" "$UUID/taxon/$file" )
    done
    taxon_files=$( printf "\"%s\"\n" ${taxon_files[@]} | jq -s '.' )
  else
    taxon_files=null
  fi

  source_files_obj=$(jq \
      -n --argjson specimen_files "$specimen_files" \
      -n --argjson multimedia_files "$multimedia_files" \
      -n --argjson taxon_files "$taxon_files" \
      '{
        "specimen": $specimen_files,
        "multimedia": $multimedia_files,
        "taxon": $taxon_files
      }'
    )

  # Remove null items
  source_files_obj=$(echo $source_files_obj | jq 'del(.[] | nulls)')
}


save_job_file() {
  jq \
      -n --arg id "$id" \
      -n --arg data_supplier "$data_supplier" \
      -n --arg date "$date_iso_8601" \
      -n --arg status "$status" \
      -n --arg tabula_rasa $tabula_rasa \
      -n --arg test_run $test_run \
      -n --argjson source_dirs "$source_dir_obj" \
      -n --argjson source_files "$source_files_obj" \
      $compact \
  '{
    "id": "\($id)",
    "data_supplier": "\($data_supplier)",
    "date": "\($date)",
    "status": "\($status)",
    "tabula_rasa": $tabula_rasa | test("true"),
    "test_run": $test_run | test("true"),
    "source_directories": $source_dirs,
    "source_files": $source_files
  }' \
  > $tmp_dir/$1 || exit_on_error "Failed to create job file!"
}


### Main 

load_script_props

## Command Line Parameters

# Extra logging
if [ ! -z $1 ] && [ $1 = "--debug" ]; then
  debug=true
  warn=true
fi
if [ ! -z $1 ] && [ $1 = "--warn" ]; then
  warn=true
fi

is_config_available

# Check configuration

log_debug "checking the configuration file: $DATA_SUPPLIERS_CONFIG_FILE"

# Verify if the config file is valid YAML
test=$( yq eval 'true' $DATA_SUPPLIERS_CONFIG_FILE )
if [ $? -gt 0 ]; then
  exit_on_error "invalid yaml! Execution ended"
fi

# Check if data suppliers have been defined
data_suppliers=$( yq eval '.data_suppliers[].name' $DATA_SUPPLIERS_CONFIG_FILE)
if [ ${#data_suppliers} -eq 0 ]; then
  exit_on_error "no data suppliers defined. Check the config file!"
fi

# Check doc types and paths
for supplier in $data_suppliers;
do
  log_debug "Data supplier: $supplier"
  reset_doc_type_source

  input_dir=$( yq eval '.data_suppliers[] | select(.name == "'$supplier'") | .path' $DATA_SUPPLIERS_CONFIG_FILE )

  # Strip trailing /
  if [ ${input_dir: -1} == "/" ]; then
    input_dir=${input_dir::-1}
  fi
  log_debug "Reading source files from: $input_dir"

  doc_types=$(yq eval '.data_suppliers[] | select(.name == "'$supplier'") | .doc_type[]' $DATA_SUPPLIERS_CONFIG_FILE )
  set_doc_type_source $doc_types

  # NOTE: when one of the directories (belonging to the document types attributed
  # to this supplier) is empty, we'll consider the upload as "not ready yet".
  # This means that even if a supplier has no documents of a specific document type,
  # the "upload_ready" file should be present in the directory if that doc type.

  if  [ $multimedia_source == true ] && [ ! -e "$input_dir/multimedia/upload_ready" ]; then
      continue
  fi

  if  [ $specimen_source == true ] && [ ! -e "$input_dir/specimen/upload_ready" ]; then
    continue
  fi

  if  [ $taxon_source == true ] && [ ! -e "$input_dir/taxon/upload_ready" ]; then
    continue
  fi

  log_info "creating job for $supplier"
  # Initialize the required job variables
  set_date
  set_id $supplier

  # Move the source files from the input folder to a temporary job folder
  create_new_job_dir
  move_source_files
  set_source_data

  # Save the job file
  job_file=$id.json
  save_job_file $job_file

  # Upload job file and source file to AWS S3
  upload_job.sh $job_file $UUID
  if [ $? -gt 0 ]; then
    log_error "upload of job $id to AWS failed!"
    restore_source_files
  else
    archive_source_files
  fi

done

log_info "finished with $errors error(s) and $warnings warning(s)"
exit $errors
