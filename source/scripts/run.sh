#!/bin/bash
# 
# run.sh : utility scripts for running the three main tasks
# of this Steward: 
# pull_repo.sh     to retrieve the configuration
# deploy_dirs.sh   to create or update the directory structure
# prepare_jobs.sh  to check for new data and create jobs if there is
#
# Usage: ./run.sh
# 
# Extra parameter:
# --debug : show debug messages in log
# 
# 
# Tom Gilissen
# Januari 2021

source functions.sh

debug=false

if [ ! -z $1 ] && [ $1 = "--debug" ]; then
  debug=true
fi

log_info "Steward 1/3: deploy directories"
deploy_dirs.sh $1

log_info "Steward 2/3: check for new data"
prepare_jobs.sh $1

log_info "Steward 3/3: clean up stopped containers"
clean_up.sh
