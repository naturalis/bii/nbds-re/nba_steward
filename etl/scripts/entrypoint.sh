#!/bin/bash
#
# entrypoint.sh : the docker image main command
#
# Tom Gilissen
# October 2021

source functions.sh

# Verify if the mandatory environment variables have been provided

if [[ -z $AWS_S3_ACCESS_KEY_ID ]]; then
    exit_on_error "Missing env variable: AWS_S3_ACCESS_KEY_ID"
fi
if [[ -z $AWS_S3_SECRET_ACCESS_KEY ]]; then
    exit_on_error "Missing env variable: AWS_S3_SECRET_ACCESS_KEY"
fi
if [[ -z $AWS_S3_DEFAULT_REGION ]]; then
    exit_on_error "Missing env variable: AWS_S3_DEFAULT_REGION"
fi
if [[ -z $AWS_S3_BUCKET ]]; then
    exit_on_error "Missing env variable: AWS_S3_BUCKET"
fi
if [[ -z $GITLAB_CONFIG_PRIVATE_KEY_BASE64 ]]; then
    exit_on_error "Missing env variable: GITLAB_CONFIG_PRIVATE_KEY_BASE64"
fi
if [[ -z $GITLAB_CONFIG_REPO ]]; then
    exit_on_error "Missing env variable: GITLAB_CONFIG_REPO"
fi
if [[ -z $MINIO_ETL_DIR ]]; then
    exit_on_error "Missing env variable: MINIO_ETL_DIR"
fi
if [[ -z $MINIO_ETL_DATA_DIR ]]; then
    exit_on_error "Missing env variable: MINIO_ETL_DATA_DIR"
fi
  

# Set the environment variables for all users

echo "AWS_S3_ACCESS_KEY_ID=$AWS_S3_ACCESS_KEY_ID" >> /etc/environment
echo "AWS_S3_SECRET_ACCESS_KEY=$AWS_S3_SECRET_ACCESS_KEY" >> /etc/environment
echo "AWS_S3_DEFAULT_REGION=$AWS_S3_DEFAULT_REGION" >> /etc/environment
echo "AWS_S3_BUCKET=$AWS_S3_BUCKET" >> /etc/environment
echo "GITLAB_CONFIG_REPO=$GITLAB_CONFIG_REPO" >> /etc/environment
echo "GITLAB_CONFIG_PRIVATE_KEY_BASE64=$GITLAB_CONFIG_PRIVATE_KEY_BASE64" >> /etc/environment
echo "MINIO_ETL_DIR=$MINIO_ETL_DIR" >> /etc/environment
echo "MINIO_ETL_DATA_DIR=$MINIO_ETL_DATA_DIR" >> /etc/environment

# Start the run once job

# Initialise the cron log file
touch /etc/steward/log/steward_etl.log

# (re)create the directory structure
log_info "(re)creating directory structure"
deploy_dirs.sh

# Setup the cron schedule
crontab /home/ubuntu/crontab
# ... and start cron
cron -f &

log_info "container has started"

# Follow the log file so it shows up in the container log
tail --follow /etc/steward/log/steward_etl.log
