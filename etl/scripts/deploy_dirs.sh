#!/bin/bash
#
# deploy_dirs.sh : creates the directories necessary for the source
# files that need to go the ETL module of the NBA
#
# Tom Gilissen
# October 2021

source functions.sh

errors=0


# Create MINIO_ETL_DIR sub dirs
info=$( mkdir -p $MINIO_ETL_DIR/{errors,in_progress,tmp} )
if [ $? -ne 0 ]
then
  log_error "(re)creating ETL process directories failed: $info"
  errors=true
fi

# Create MINIO_ETL_DATA_DIR sub dirs
info=$( mkdir -p $MINIO_ETL_DATA_DIR )
if [ $? -ne 0 ]
then
  log_error "(re)creating the ETL data directory failed: $info"
  errors=true
fi

info=$( mkdir -p $MINIO_ETL_DATA_DIR/{brahms,col,crs,dcsr,nsr}/{archive,new,reports} )
if [ $? -ne 0 ]
then
  log_error "(re)creating ETL source directories failed: $info"
  errors=true
fi

info=$( mkdir -p $MINIO_ETL_DATA_DIR/{geoareas,medialib}/{archive,new,reports} )
if [ $? -ne 0 ]
then
  log_error "(re)creating ETL geoareas/medialib directories failed: $info"
  errors=true
fi


log_info "finished with $errors error(s) and $warnings warning(s)"
exit $errors
