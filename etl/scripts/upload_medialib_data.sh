#!/bin/bash
#
# upload_medialib_data.sh checks for new update files containing
# either:
#
# - medialib ids : file with filename medialib_ids_cache.zip in the directory ids (../ids/medialib_ids_cache.zip); or
# - mimetypes    : file with filename mimetypes.zip in the directory mimetypes (../mimetypes/mimetypes.zip)
#
# When one of these files is available, and the upload_ready file signalling upload completion, it will be tar-ed and
# uploaded to a specifief AWS S3 bucket. There it can later be collected by a another process. When the upload is
# succesful, the tar-file will be moved to the archive directory.
#
# Usage: ./upload_medialib_data.sh
#
# Optional parameter:
# --debug : include debug messages
#
#
# Tom Gilissen
# June 2021


alias aws='docker run --rm -i -v $MINIO_ETL_DIR:/aws \
         --env AWS_ACCESS_KEY_ID=$AWS_S3_ACCESS_KEY_ID \
         --env AWS_SECRET_ACCESS_KEY=$AWS_S3_SECRET_ACCESS_KEY \
         --env AWS_DEFAULT_REGION=$AWS_S3_DEFAULT_REGION \
         --env AWS_BUCKET=$AWS_S3_BUCKET \
         amazon/aws-cli'

shopt -s expand_aliases

source functions.sh


### Variables and Constants

debug=false
UUID=$( uuidgen )

SOURCE_DIR=/data/etl/naturalis/medialib
PROCESS_DIR=/data/etl/tmp

IDS="ids"
IDS_FILE=medialib_ids_cache.zip
MIMETYPES="mimetypes"
MIMETYPES_FILE=mimetypes.zip


### Functions

# Create temporary dir for file processing
function mk_tmp() {
  tmp=$PROCESS_DIR/$UUID
  mkdir $tmp
  log_debug "Created tmp dir: $tmp"
}

# Remove temporary dir
function rm_tmp() {
  rm -fR $PROCESS_DIR/$UUID
  log_debug "Removed tmp dir: $tmp"
}

# Upload the update file of the given type to the AWS S3 storage
function upload_update() {

  update_type=$1
  if [ $update_type != $IDS ] && [ $update_type != $MIMETYPES ]; then
    exit_on_error "Unknown update type: $update_type. Please use either '$IDS' or '$MIMETYPES'. Update failed!"
  fi
  if [ $update_type == $IDS ]; then
    update_file=$IDS_FILE
  else
    update_file=$MIMETYPES_FILE
  fi
  log_info "Processing new medialib $update_type update file: $update_file"

  # Prepare archive file for upload to S3 data dir
  mk_tmp
  copied=$( cp $SOURCE_DIR/new/$update_type/$update_file $tmp/. )
  archive_file=medialib-$update_type-update-$(date +"%Y-%m-%d-%s").tar.gz
  archive_cmd=$( tar czvf $PROCESS_DIR/$archive_file -C $tmp . )
  if [ ! -f "$PROCESS_DIR/$archive_file" ]; then
      exit_on_error "Failed to archive $update_type update file: $update_file. Update has not been processed!"
  fi
  
  # Upload archive file to S3 data dir
  # NOTE: path to aws source file is relative to $MINIO_ETL_DIR!
  aws s3 cp tmp/$archive_file $AWS_S3_BUCKET/medialib/$archive_file
  error=$?
  if [ $error -gt 0 ]; then
    rm_tmp
    rm $PROCESS_DIR/$archive_file
    exit_on_error "Upload of $update_file ($archive_file) to AWS failed"
  fi

  # Cleanup once the upload is succesful
  mv $PROCESS_DIR/$archive_file $SOURCE_DIR/archive/.
  removed=$( rm $SOURCE_DIR/new/$update_type/* )
  rm_tmp
  log_info "Update file $update_file has been archived and uploaded to the S3 data store"
}


### Main

# Debug logging
if [ ! -z $1 ] && [ $1 = "--debug" ]; then
	debug=true
fi


# 1. ids update
log_info "Checking for update medialib $IDS"

if [ -e "$SOURCE_DIR/new/$IDS/upload_ready" ] && [ -e "$SOURCE_DIR/new/$IDS/$IDS_FILE" ]
then
  log_debug "Processing new medialib $IDS update file: $IDS_FILE"
  upload_update $IDS
else
  log_info "No new $IDS update file"
fi


# 2. mimetypes update
log_info "Checking for update medialib $MIMETYPES"

if [ -e "$SOURCE_DIR/new/$MIMETYPES/upload_ready" ] && [ -e "$SOURCE_DIR/new/$MIMETYPES/$MIMETYPES_FILE" ]
then
  log_debug "Processing new medialib $MIMETYPES update file: $MIMETYPES_FILE"
  upload_update $MIMETYPES
else
  log_info "No $MIMETYPES update file"
fi
