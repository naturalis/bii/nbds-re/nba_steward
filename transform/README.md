## Steward: Transform

## Directory structure

On host:

/data

/data/es-data

/data/etl
/data/etl/done
/data/etl/incoming
/data/etl/reports
/data/etl/temporary

/data/export

/data/logs

/data/source-data/
/data/source-data/brahms
/data/source-data/col
/data/source-data/crs
/data/source-data/dcsr
/data/source-data/geo
/data/source-data/json-imports
/data/source-data/medialib
/data/source-data/ndff
/data/source-data/nsr

/data/steward/
/data/steward/done
/data/steward/in_progress
/data/steward/medialib
/data/steward/new
/data/steward/temporary

/opt/compose_projects/
/opt/compose_projects/etl/
/opt/compose_projects/etl/compose
/opt/compose_projects/etl/config


In docker container:

/payload/data
/payload/data/brahms
/payload/data/col
/payload/data/crs
/payload/data/dcsr
/payload/data/geo
/payload/data/json-imports
/payload/data/medialib
/payload/data/ndff
/payload/data/nsr

/payload/software
/payload/software/conf
/payload/software/conf/thematic-search

/payload/software/export
/payload/software/lib
/payload/software/log
/payload/software/sh


`/payload/software/conf` contains ETL configuration files:
- log4j2.xml
- nba.properties

It also contains a clone of the repo https://github.com/naturalis/nba-brondata-bijzcol: `/thematic-search`

`thematic-search_last_update.log` is a file which logs the datetimes of repo checkouts.


`/payload/software/lib` contains the ETL jar: `nba-etl.jar`

`/payload/software/sh` contains the ETL shell script


Mapping host > container:

/data/source-data                        >  /payload/data
/data/export                             >  /payload/software/export
/data/logs                               >  /payload/software/log
/home/ubuntu/etl-config/thematic-search  >  /payload/software/conf/thematic-search
/home/ubuntu/etl-config                  >  /payload/software/conf



## Container configuration

working directory: /payload/software/sh

