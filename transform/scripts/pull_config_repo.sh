#!/bin/bash
#
# pull_config_repo.sh : script for pulling the repo passed as 
# environment variable GITLAB_CONFIG_REPO_FILE

log_info() {
  echo "$(date -Iseconds)|INFO |pull_config_repo.sh: $1"
}

log_error() {
	echo "$(date -Iseconds)|ERROR|pull_config_repo.sh: $1" 1>&2
}

mkdir -p /etc/steward

# checkout repo but empty directory first so clone will not fail
if [ ! -d /etc/steward/config/.git ]
then
  rm -rf /etc/steward/config/*
fi

# clone or pull
if ! git clone "${GITLAB_CONFIG_REPO}" /etc/steward/config > /dev/null 2>&1 || git -C /etc/steward/config reset --hard > /dev/null 2>&1 && git -C /etc/steward/config pull > /dev/null 2>&1;
then
	log_error "pulling repo $GITLAB_CONFIG_REPO failed!"
	exit $?
fi
