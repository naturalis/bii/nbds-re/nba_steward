#!/bin/bash
# 
# run.sh : utility scripts for running the three main tasks
# of this Steward: 
# deploy_dirs.sh   to create or update the directory structure
# pull_jobs.sh     to pull new jobs
# transform.sh     *** not ready yet ***
# push_job.sh      push job with finished result of transformation
#
# Usage: ./run.sh
# 
# Extra parameter:
# --debug : show debug messages in log
# 
# 
# Tom Gilissen
# Februari 2021

# shellcheck source=/dev/null
source functions.sh

debug=false

if [ -n "$1" ] && [ "$1" = "--debug" ]; then
  debug=true
fi

if [ $debug ]; then
  log_debug "debug status: $debug"
fi

log_info "Steward Transform 1/5: pull medialib updates"
pull_medialib_updates.sh "$1"

log_info "Steward Transform 2/5: check for new jobs"
pull_new_jobs.sh "$1"

log_info "Steward Transform 3/5: move new job to ETL"
move_job_to_etl.sh "$1"

log_info "Steward Transform 4/5: push finished job"
push_job.sh "$1"

log_info "Steward Transform 5/5: clean up stopped containers"
clean_up.sh "$1"
