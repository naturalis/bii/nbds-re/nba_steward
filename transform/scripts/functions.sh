#!/bin/bash
#
# functions.sh : shared functions
#
# Tom Gilissen
# February 2021

function log_debug() {
    echo "$(date -Iseconds)|DEBUG|$(basename -- "$0"): $1"
}

function log_error() {
	(( errors++ )) || true
	echo "$(date -Iseconds)|ERROR|$(basename -- "$0"): $1" 1>&2
}

function log_info() {
  echo "$(date -Iseconds)|INFO |$(basename -- "$0"): $1"
}

function log_warning() {
	(( warnings++ )) || true
	if [ "${warn:?}" ]; then
		echo -e "$(date -Iseconds)|WARN |$(basename -- "$0"): $1"
	fi
	if [ "${fail_on_warning:=false}" ]; then
		exit "$warnings"
	fi
}

function exit_on_error() {
  log_error "$1"
  exit "$errors"
}
