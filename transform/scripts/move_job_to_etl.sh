#!/bin/bash
# move_job_to_etl.sh
#
#
#
# required .env variables:
# STEWARD_DATA_DIR=/data/steward
# ETL_DATA=/data/etl
#
# Tom Gilissen
# Februari 2021

# shellcheck source=/dev/null
source functions.sh

alias aws='docker run --rm -i -v $STEWARD_DATA_DIR:/aws '\
'--env AWS_ACCESS_KEY_ID=$AWS_S3_ACCESS_KEY_ID '\
'--env AWS_SECRET_ACCESS_KEY=$AWS_S3_SECRET_ACCESS_KEY '\
'--env AWS_DEFAULT_REGION=$AWS_S3_DEFAULT_REGION '\
'--env AWS_BUCKET=$AWS_S3_BUCKET '\
'amazon/aws-cli'

shopt -s expand_aliases

### Variables and Constants

debug=false
warn=false
fail_on_warning=false
errors=0
warnings=0

job_file=
job_file_name=


### Main

# Extra logging
if [ -n "$1" ] && [ "$1" = "--debug" ]; then
  debug=true
  warn=true
fi
if [ -n "$1" ] && [ "$1" = "--warn" ]; then
  warn=true
fi
if [ $debug ]; then
  log_debug "debug: $debug"
  log_debug "warn: $warn"
  log_debug "fail on warning: $fail_on_warning"
fi


# 1. Select the oldest job file from the steward new jobs folder (/data/steward/incoming)
mapfile -t files < <( ls "$STEWARD_DATA_DIR"/incoming/*.json -atr 2> /dev/null )
if [ ${#files[@]} -gt 0 ]; then
  job_file=${files[0]}
  job_file_name=${job_file#"$STEWARD_DATA_DIR"/incoming/}
  log_info "job file selected: $job_file_name"
else
  log_debug "no job files: nothing to do"
  log_info "finished with $errors error(s) and $warnings warning(s)"
  exit 0
fi

# 2. Mark the job file ass "in progress"
mv "$job_file" "$job_file.inprogress"
job_file="$job_file.inprogress"
log_debug "job file in progress: $job_file"

# info: retrieve supplier info
supplier=$( jq --raw-output '.data_supplier' < "$job_file" )
log_debug "data supplier: $supplier"

# info: retrieve directory name of the source data
dir=$( jq --compact-output --raw-output '.source_directory' < "$job_file" )
log_debug "data directory: $dir"

# 3. Sync aws s3 directories (aws:/etl/<dir>) to local file system: /data/steward/temporary/<dir>
# Note: aws client's base dir is $STEWARD_DATA_DIR
log_debug "synchronising aws directory /etl/$dir to: $STEWARD_DATA_DIR/temporary/$dir"
if ! aws s3 sync "$AWS_S3_BUCKET/etl/$dir/" "./temporary/$dir";
then
  exit_on_error "sync of source folder $dir failed"
fi

# 4. Move the directory containing the source files to the etl incoming directory
log_debug "move $STEWARD_DATA_DIR/temporary/$dir to $ETL_DATA_DIR/incoming/."
if ! mv "$STEWARD_DATA_DIR/temporary/$dir" "$ETL_DATA_DIR"/incoming/.;
then
  exit_on_error "moving source folder $dir failed"
fi

# 5. Unzip archive file(s) containing source data
while IFS= read -r -d '' file
do
  log_debug "extracting archive: ${file}"
  tar xzf "${file}" --directory "${ETL_DATA_DIR}/incoming/${dir}"
  rm "${file}"
done < <(find "${ETL_DATA_DIR}/incoming/${dir}" -maxdepth 1 -name "*.tar.gz" -print0)

# 6. Copy the job file to the ETL incoming directory
log_debug "copy ${job_file} to ${ETL_DATA_DIR}/incoming/${job_file_name}"
if [ ! -d "${ETL_DATA_DIR}/incoming/" ]; then
  mkdir "${ETL_DATA_DIR}/incoming/" -p
fi
if ! cp "${job_file}" "${ETL_DATA_DIR}/incoming/${job_file_name}";
then
  log_error "failed to copy job file \'${job_file_name}\' to directory \'incoming\'"
fi

# 7. Move the job file to the Steward in_progress directory
log_debug "moving job file ${job_file} to: ${STEWARD_DATA_DIR}/in_progress/${job_file_name}"
if [ ! -d "${STEWARD_DATA_DIR}/in_progress" ]; then
  mkdir "${STEWARD_DATA_DIR}/in_progress" -p
fi
if ! mv "${job_file}" "${STEWARD_DATA_DIR}/in_progress/${job_file_name}";
then
  log_error "failed to move job file \'${job_file_name}\' to directory \'in_progress\'"
fi

log_info "moved job ${job_file_name} to the etl for further processing"
log_info "finished with ${errors} error(s) and ${warnings} warning(s)"
exit ${errors}
