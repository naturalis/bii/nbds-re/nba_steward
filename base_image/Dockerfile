FROM ubuntu:20.04
LABEL maintainer="Tom Gilissen tom.gilissen@naturalis.nl"

ENV STEWARD_VERSION 1.0
ENV UBUNTU_VERSION 20.04

RUN set -eu

# Add packages

# Prevent tzdata from prompting to set the default timezone:
ENV DEBIAN_FRONTEND=noninteractive 
RUN apt-get update && \
    apt-get install -y apt-utils unattended-upgrades && \
    unattended-upgrade && \
    apt-get install -y --no-install-recommends cron curl git indent software-properties-common ssh uuid-runtime vim wget && \
    apt-get autoremove

# Install docker client binary
ARG DOCKER_CLI_VERSION="20.10.2"
ENV DOWNLOAD_URL="https://download.docker.com/linux/static/stable/x86_64/docker-$DOCKER_CLI_VERSION.tgz"
RUN mkdir -p /tmp/download \
    && curl -L $DOWNLOAD_URL | tar -xz -C /tmp/download \
    && mv /tmp/download/docker/docker /usr/local/bin/ \
    && rm -rf /tmp/download

# Create directory structure
RUN /bin/bash -c "mkdir -p /usr/local/bin/mc /usr/local/bin/jq /usr/local/bin/yq /usr/local/bin/steward /etc/steward /etc/steward/config /data"
ENV PATH="$PATH:/usr/local/bin/steward/"

# MinioClient
RUN wget -P /usr/local/bin/mc https://dl.min.io/client/mc/release/linux-amd64/mc
RUN chmod 755 /usr/local/bin/mc/mc
ENV PATH "$PATH:/usr/local/bin/mc"

# jq
RUN wget https://github.com/stedolan/jq/releases/download/jq-1.5/jq-linux64
RUN mv jq-linux64 /usr/local/bin/jq/jq
RUN chmod 755 /usr/local/bin/jq/jq
ENV PATH "$PATH:/usr/local/bin/jq"

# yq
RUN add-apt-repository ppa:rmescandon/yq
RUN apt-get update && apt-get install yq -y
