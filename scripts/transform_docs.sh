#!/bin/bash
#
# transform_docs.sh - Run the ETL module from the NBA 
# to transform records from the source files into 
# valid NBA documents
#
# Usage: ./transform_docs.sh [job_id].json
#
# Author: Tom Gilissen
# Date: April 2020 


# Variables

job_file=
job_file_orig=

data_supplier=
declare -a files=

outset_jobs_dir=/data/colander/outset/jobs
outset_jobs_archive=/data/colander/outset/jobs_archive
outset_source_files_dir=/data/colander/outset/source_files

etl_jobs_dir=/data/colander/validator/jobs
etl_source_files_dir=/data/colander/validator/source_files

validator_jobs_dir=/data/colander/validator/jobs
validator_source_files_dir=/data/colander/validator/source_files

server=minio-nba-bron


# Functions

validate_json() {
	cat $1 | jq type || exit_on_error "Failed to parse file: $1! Aborting"
}

exit_on_error() {
	echo "ERROR: $1" 1>&2 # send standard output to standard error
	mv $job_file $job_file_orig".failed"
	exit 1
}

usage() {
	echo "INFO: Usage: ./run_etl.sh [job-id].json"
}

set_job_file() {
	job_file=$outset_jobs_dir/$1
	job_file_orig=$job_file
	if [ ! -r $job_file ]
	then
		echo "ERROR: job file does not exist or cannot be read!"
		echo "Please make sure the job file is in the directory: $outset_jobs_dir"
		exit 1
	fi
}

set_job_inprogress() {
	# Add .inprogress to the file name of the job file
	mv $job_file "$job_file.inprogress"
	job_file="$job_file.inprogress"
}

set_job_id() {
	job_id=$(jq -r '.id' $job_file) || exit_on_error "Failed to retrieve id from job file: $job_file_orig"
	echo "INFO: Job id: $job_id"
}

set_data_supplier() {
	data_supplier=$( (jq -r '.data_supplier' $job_file) | tr '[:upper:]' '[:lower:]' )
	echo "INFO: Data supplier: $data_supplier"

	case $data_supplier in
		crs)
			dir=brondata-crs
			;;
		brahms)
			dir=brondata-brahms
			;;
		*)
			exit_on_error "Unknown data supplier: $data_supplier"
			;;
	esac
}

set_source_files() {
	# Create an array containing the source file(s)
	files=( $(jq -r '.files[]' $job_file) )
	size=${#files[@]}
}

reset_job_status() {
	# Remove the .inprogress extension from the file name 
	mv $job_file $job_file_orig || exit_on_error "Failed to reset job file from \"$job_file\" to \"$job_file_orig\"! Aborting"
}


##### Main

args_count=$#
if [ $args_count -eq 0 ] || [ $args_count -gt 1 ]
then
	echo "Number of parameters does not match." 
	usage
	exit 1
fi

set_job_file $1
set_job_inprogress
validate_json $job_file
set_job_id
set_data_supplier
set_source_files

reset_job_status