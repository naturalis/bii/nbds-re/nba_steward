#!/bin/bash
# run_jobs.sh

echo "jobs waiting to be downloaded:"
ls /data/colander/outset/jobs -tr

echo "jobs for etl module:"
ls /data/colander/etl/jobs -tr

echo "jobs for the validator:"
ls /data/colander/validator/jobs -tr

echo "jobs for the infuser:"
ls /data/colander/infuser/jobs -tr