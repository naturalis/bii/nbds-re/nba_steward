#!/bin/bash
#
# initiate_job.sh - This is where the conveyer belt which the colander essentially is,
# starts running: with the creation of a job. For a job to be initiated two pieces of 
# information are needed:
# - the source system
# - the source files
#  
# For example:
# ./initiate_job.sh -s CRS -f crs-specimens-2020-03-24.tar.gz crs-multimedia-2020-03-24.tar.gz
# ./initiate_job.sh -s CRS -f crs-specimens-2020-03-24.tar.gz crs-multimedia-2020-03-24.tar.gz -m metadata-specimen.json metadata-multimedia.json
# ./initiate_job.sh --sourcesystem CRS --files crs-specimens-2020-03-24.tar.gz crs-multimedia-2020-03-24.tar.gz --metadata metadata-specimen.json metadata-multimedia.json
# 
# Parameters:
# -s, --source : SourceSystem
# -f, --files : one or more source files
# -m. --metadata : one or more files containing metadata about the export
# -t, --test : run as test. Files will only be validated
#
# Author: Tom Gilissen
# Date: March 2020 


##### Constants and Variables

data_supplier=
date_iso_8601=
id=
declare -a source_files
declare -a metadata_files

# source_files_obj=null
# metadata_files_obj=null

status="job created"
test_run=false
tabula_rasa=false
is_incremental=false

jobs_dir=/data/colander/outset/jobs
source_files_dir=/data/colander/outset/source_files

#compact="--compact-output"
compact=

##### Functions

set_data_supplier() {
  data_supplier=${1^^} # to uppercase
  local accepted=false
  sources=(BRAHMS CRS COL NSR OBS XC)
  for source in ${sources[@]}
  do
    if [[ "$data_supplier" == "$source" ]] ; then 
      accepted=true
      echo "INFO: data supplier: $data_supplier"
      break
    fi
  done
  if [[ "$accepted" == "false" ]] ; then
    exit_on_error "Unknown data supplier: $data_supplier. Could NOT initiate a new job!"
  fi
}

set_date() {
  date_iso_8601=`date --iso-8601=seconds`
  echo "INFO: job date: $date_iso_8601"
}

set_id() {
  local timestamp=$(date +%s%3N)
  local miliseconds=${timestamp:(-3)}
  local datetime=$(date +%Y-%m-%d-%H%M%S$miliseconds)
  local hash=$(echo "${data_supplier,,}$timestamp" | sha256sum)
  local hash=${hash:0:8} # use only the first 8 characters
  id=${data_supplier,,}-$datetime-$hash
  echo "INFO: job id: $id"
}

set_source_files_obj() {

  local -a specimen_files
  local -a multimedia_files
  local -a taxon_files
  local -a geoarea_files

  # What is the document type
  for file in ${source_files[@]}
  do
    if [[ $file == *"specimen"* ]]
    then
      specimen_files=( "${specimen_files[@]}" "$file" )
    elif [[ $file == *"multimedia"* ]]
    then
      multimedia_files=( "${multimedia_files[@]}" "$file" )
    elif [[ $file == *"taxon"* ]]
    then
      taxon_files=( "${taxon_files[@]}" "$file" )
    elif [[ $file == *"geoarea"* ]]
    then
      geoarea_files=( "${geoarea_files[@]}" "$file" )
    else
      exit_on_error "Failed to establish document type belonging to file $file. Aborting!"
    fi
  done

  # Create an array per document type
  if [[ ${#specimen_files[@]} -gt 0 ]]
    then
      specimen_files=$( printf "\"%s\"\n" ${specimen_files[@]} | jq -s '.' )
    else
      specimen_files=null
  fi
  if [[ ${#multimedia_files[@]} -gt 0 ]]
    then
      multimedia_files=$( printf "\"%s\"\n" ${multimedia_files[@]} | jq -s '.' )
    else
      multimedia_files=null
  fi
  if [[ ${#taxon_files[@]} -gt 0 ]]
    then
      taxon_files=$( printf "\"%s\"\n" ${taxon_files[@]} | jq -s '.' )
    else
      taxon_files=null
  fi
  if [[ ${#geoarea_files[@]} -gt 0 ]]
    then
      geoarea_files=$( printf "\"%s\"\n" ${geoarea_files[@]} | jq -s '.' )
    else
      geoarea_files=null
  fi

  source_files_obj=$(jq \
    -n --argjson specimen_files "$specimen_files" \
    -n --argjson multimedia_files "$multimedia_files" \
    -n --argjson taxon_files "$taxon_files" \
    -n --argjson geoarea_files "$geoarea_files" \
    '{
      "specimen": $specimen_files,
      "multimedia": $multimedia_files,
      "taxon": $taxon_files,
      "geoarea": $geoarea_files
    }'
  )
  # Remove null items
  source_files_obj=$(echo $source_files_obj | jq 'del(.[] | nulls)')
}

set_metadata_files_obj() {

  local obj='{"metadata_files": {}}'
  
  for file in ${metadata_files[@]}
  do
    case $file in 
      metadata*specimen*)
      obj=$(echo $obj | jq '.metadata_files += {specimen: "'$file'"}')
      ;;
      metadata*multimedia*)
      obj=$(echo $obj | jq '.metadata_files += {multimedia: "'$file'"}')
      ;;
      metadata*taxon*)
      obj=$(echo $obj | jq '.metadata_files += {taxon: "'$file'"}')
      ;;
      metadata*geoarea*)
      obj=$(echo $obj | jq '.metadata_files += {geoarea: "'$file'"}')
      ;;
      *)
      echo "WARNING: unable to establish document type from metadata file $file. Skipping file!"
    esac
  done
  
  metadata_files_obj=$(echo $obj | jq '.metadata_files | del(.[] | nulls)')
}

save_job_file() {
  jq \
      -n --arg id "$id" \
      -n --arg data_supplier "$data_supplier" \
      -n --arg date "$date_iso_8601" \
      -n --arg status "$status" \
      -n --arg tabula_rasa $tabula_rasa \
      -n --arg test_run $test_run \
      -n --argjson files "$source_files" \
      -n --argjson metadata_files "$metadata_files" \
      $compact \
  '{
    "id": "\($id)",
    "data_supplier": "\($data_supplier)",
    "date": "\($date)",
    "status": "\($status)",
    "tabula_rasa": $tabula_rasa | test("true"),
    "test_run": $test_run | test("true"),
    "source_files": $files,
    "metadata_files": $metadata_files
  }' \
  > $jobs_dir/$1 || exit_on_error "Failed to create job file!"
}

exit_on_error() {
  echo "ERROR: $1" 1>&2
  exit 1
}

usage() {
  echo "Usage: ./initiate_job.sh -s [BRAHMS|CRS|COL|NSR|XC|OBS] -f [source_file(s)] -m [metadata_file(s)]"
}


##### Main

# Digest arguments

while [ "$1" != "" ]; do
  case $1 in
    -s | --sourcesystem ) shift
                      set_data_supplier $1
                      ;;
    -f | --files )    shift
                      i=0
                      while [[ ("$1" != "") && ("$1" != -*) ]]; do
                        source_files[$i]=$1
                        i=$(($i + 1))
                        shift
                      done
                      ;;
    -m | --metadata ) shift
                      i=0
                      while [[ ("$1" != "") && ("$1" != -*) ]]; do
                        # TODO: include file name check
                        metadata_files[$i]=$1
                        i=$(($i + 1))
                        shift
                      done
                      ;;
    -t | --testrun )  shift
                      test_run=true
                      ;;
    -h | --help )     usage
                      exit
                      ;;
    * )               usage
                      exit 1
  esac
  if [[ "$1" != -* ]];
  then
      shift
  fi
done

data_supplier=$source_system

# Check for required arguments
if [[ "$data_supplier" = "" ]];
then
    echo "ERROR: Please provide a source system"
    usage
    exit 1
fi

# if [ ${#source_files[*]} = 0 ];
# then
#   echo "ERROR: Please provide at least one source file"
#   usage
#   exit 1
# fi

# # Create an array containing the source file(s)
# source_files=$( printf "\"%s\"\n" ${source_files[@]} | jq  --compact-output -s '.' )
# echo "INFO: source files: $source_files"

# # Optional arguments
# if [ ${#metadata_files[*]} != 0 ];
# then
#   metadata_files=$( printf "\"%s\"\n" ${metadata_files[@]} | jq --compact-output -s '.' )
#   echo "INFO: metadata files: $metadata_files"
# else
#   metadata_files='[]'
# fi

# Initialize the required job variables
set_date
set_id
# set_source_files_obj
# set_metadata_files_obj 
job_file=$id.json

# Save the job file
save_job_file $job_file

echo "INFO: job file: $job_file"
